#!/bin/bash

function get_plugin ()
{
       if [ ! -d "${1}" ]; then
               echo "Cloning ${1}"
               git clone "git://repo.or.cz/${1}"
               # generate the configure in the subdir
               cd "${1}"
       else
               echo "Updating ${1}"
               cd "${1}"
               git pull

               if [ $? -ne 0 ]; then
                       cd "${OLDPWD}"
                       rm -rfv "${1}"
                       get_plugin "${1}"
               fi
        fi

       NOCONFIGURE=yes ./autogen.sh
       cd ${OLDPWD}
}

function get_hg_cmake ()
{
       if [ ! -d "${1}" ]; then
               echo "Cloning ${1}"
               hg clone "${2}" "${1}"
               # generate the configure in the subdir
               cd "${1}"
       else
               echo "Updating ${1}"
               cd "${1}"
               hg pull
               hg update

               if [ $? -ne 0 ]; then
                       cd "${OLDPWD}"
                       rm -rfv "${1}"
                       get_hg_cmake ${1} ${2}
               fi
        fi

       cmake -D debug=on .
       cd ${OLDPWD}
}

get_plugin gmpc-alarm
get_plugin gmpc-albumview
get_plugin gmpc-avahi
get_plugin gmpc-awn
#get_plugin gmpc-coveramazon
#get_plugin gmpc-discogs
#get_plugin gmpc-extraplaylist
#get_plugin gmpc-fullscreeninfo
get_plugin gmpc-jamendo
#get_plugin gmpc-last.fm
get_plugin gmpc-libnotify
#get_plugin gmpc-lirc
get_plugin gmpc-lyrics
get_plugin gmpc-lyricwiki
get_plugin gmpc-magnatune
#get_plugin gmpc-mdcover
#get_plugin gmpc-mserver
# Disabled because of last.fm policy change
#get_plugin gmpc-lastfmradio
get_plugin gmpc-shout
get_plugin gmpc-tagedit
get_plugin gmpc-wikipedia
#get_plugin gmpc-playlistsort
#get_plugin gmpc-lyricsplugin
get_plugin gmpc-mmkeys

# get_hg_cmake gmpc-dynamicplaylist http://bitbucket.org/misery/dynamic-playlist/

aclocal
automake --gnu -f --add-missing
autoconf

echo "Running plugins configure"

if test x$NOCONFIGURE = x; then
	./configure "$@" || exit 1
fi
